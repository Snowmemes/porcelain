
CXX=g++
CXX_FLAGS=-Wall -Wextra -pedantic -std=c++11 -fsanitize=address

.PHONY: run clean

exe.out: main.cpp
	$(CXX) $(CXX_FLAGS) main.cpp -o exe.out

run: exe.out
	./exe.out

clean:
	rm -f exe.out

