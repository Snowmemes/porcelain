
#include <iostream>

typedef unsigned short u16;
typedef unsigned long u32;

using namespace std;

const int MAX_ELEMENTS = 100;

void load_input(u32& n, u32& m, u32**& sums) {
	u32 prefix_right[MAX_ELEMENTS + 1] = {0};
	u32 prefix_left[MAX_ELEMENTS + 1] = {0};
	u16 workplace[MAX_ELEMENTS] = {0};		// TODO: rename to shelf

	// # of shelves, total shrieks
	cin >> n >> m;

	for (u16 i = 0; i < n; i++) {
		u16 count;		// # of items on this shelf
		cin >> count;

		for (u16 j = 0; j < count; j++) {
			u16 value;	// value of this item
			cin >> value;
			workplace[j] = value;
		}

		// compute the prefix sums from each side
		for (u16 j = 1; j <= count; j++) {
			prefix_left [j] = workplace[j - 1]     + prefix_left [j - 1];
			prefix_right[j] = workplace[count - j] + prefix_right[j - 1];
		}

		for (u16 j = 1; j <= count; j++) {
			// find the max for the diagonal
			u16 max = 0;
			for (u16 k = 0; k < j; k++) {
				u16 a = prefix_left[j - k] + prefix_right[k];
				u16 b = prefix_left[k] + prefix_right[j - k];
				u16 value = a > b ? a : b;

				max = value > max ? value : max;
			}

			sums[i][j] = max;
		}

		// FIXME: this part is dumb, takes too long
		for (u32 j = count + 1; j <= m; j++) {
			sums[i][j] = sums[i][count];
		}
	}
}

void print_sums(u32 n, u32**& sums) {
	for (u16 i = 0; i < n; i++) {
		for (u16 j = 0; j < MAX_ELEMENTS + 1; j++) {
			cout << sums[i][j] << " ";
		}

		cout << endl;
	}
}

void solve(u32 n, u32 m, u32**& sums) {
	u32 limit = MAX_ELEMENTS < m ? MAX_ELEMENTS : m;

	for (u16 y = 1; y < n; y++) {
		u32* prev = sums[y - 1];
		u32* row = sums[y];

		for (u16 x = limit; x > 0; x--) {
			u32 max = 0;

			for (u16 _x = x; _x < x + 1; _x--) {
				u32 value = row[_x] + prev[x - _x];
				//cout << "(" << y << "x" << x << "): considering " << value << " (" << row[_x] << " + " << prev[x - _x] << ")" << endl;
				max = value > max ? value : max;
			}

			row[x] = max;
		}
	}
}

int main() {
	u32** sums = new u32*[MAX_ELEMENTS];
	u32 n = 0, m = 0;

	for (u32 i = 0; i < MAX_ELEMENTS; i++) {
		sums[i] = new u32[MAX_ELEMENTS * MAX_ELEMENTS + 1];

		for (u32 j = 0; j < MAX_ELEMENTS * MAX_ELEMENTS + 1; j++) {
			sums[i][j] = 0;
		}
	}

	load_input(n, m, sums);
	//print_sums(n, sums);

	solve(n, m, sums);
	//print_sums(n, sums);

	//cout << endl << "Solution: " << sums[n - 1][m] << endl;
	//cout << "(n = " << n << "; m = " << m << ")" << endl;
	cout << sums[n - 1][m] << endl;

	for (u16 i = 0; i < MAX_ELEMENTS; i++) {
		delete[] sums[i];
	}

	delete[] sums;

	return 0;
}
